package com.example.demo3.Eservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo3.Emodel.Employee;
import com.example.demo3.Erepository.*;


@Service
public class EService {


@Autowired
ERepository employeerepository;

public List<Employee> getAllEmployee()
{
List<Employee> employee = new ArrayList<Employee>();
employeerepository.findAll().forEach(employee1 -> employee.add(employee1));
return employee;
}

public Employee getEmployeeById(int ID)
{
return employeerepository.findById(ID).get();
}

public void saveOrUpdate(Employee employee)
{
employeerepository.save(employee);
}

public void delete(int ID)
{
employeerepository.deleteById(ID);
}

public void update(Employee employee, int ID)
{
    employeerepository.save(employee);
}
}
