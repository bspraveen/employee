package com.example.demo3.Erepository;

import org.springframework.data.repository.CrudRepository;
import com.example.demo3.Emodel.*;


public interface ERepository extends CrudRepository<Employee, Integer> {

}

